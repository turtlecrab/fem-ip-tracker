import styled from 'styled-components'

interface Props {
  ip: string
  location: string
  timezone: string
  isp: string
}

function Display({ ip, location, timezone, isp }: Props) {
  return (
    <Container>
      <Section>
        <Heading>IP Address</Heading>
        <Text>{ip}</Text>
      </Section>
      <Section>
        <Heading>Location</Heading>
        <Text>{location}</Text>
      </Section>
      <Section>
        <Heading>Timezone</Heading>
        <Text>{timezone}</Text>
      </Section>
      <Section>
        <Heading>ISP</Heading>
        <Text>{isp}</Text>
      </Section>
    </Container>
  )
}

const Container = styled.div`
  position: absolute;
  z-index: 1000;
  top: 167px;
  left: 50%;
  transform: translateX(calc(-50% - 24px));
  display: flex;
  flex-direction: column;
  justify-content: center;
  border-radius: 15px;
  background-color: white;
  width: calc(100% - 48px);
  margin: 0 24px;
  padding: 26px 24px 0;
  max-width: 555px;
  box-shadow: 0 30px 30px #0001;

  & > * + *::before {
    @media screen and (min-width: 1024px) {
      content: '';
      position: absolute;
      width: 1px;
      height: 75px;
      height: calc(100% - 12px);
      left: 0;
      top: 6px;
      background-color: var(--divider-color);
    }
  }

  @media screen and (min-width: 1024px) {
    top: 200px;
    flex-direction: row;
    padding: 37px 0;
    max-width: 1110px;
  }
`

const Section = styled.section`
  position: relative;
  text-align: center;
  margin-bottom: 24px;

  @media screen and (min-width: 1024px) {
    text-align: left;
    flex: 1;
    margin: 0;
    padding: 0 32px;
  }
`

const Heading = styled.h2`
  margin: 0;
  padding: 0;
  font-family: var(--ff);
  font-weight: 700;
  font-size: 10px;
  letter-spacing: 0.15em;
  text-transform: uppercase;
  color: var(--light-gray);

  @media screen and (min-width: 1024px) {
    font-size: 12px;
  }
`

const Text = styled.p`
  margin: 0;
  padding: 8px 0 0;
  font-family: var(--ff);
  font-weight: 500;
  font-size: 20px;
  letter-spacing: -0.01em;
  color: var(--dark-gray);

  @media screen and (min-width: 1024px) {
    font-size: 25px;
    letter-spacing: 0.01em;
    padding: 13px 0 0;
  }
`

export default Display
