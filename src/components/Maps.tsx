import { YMaps, Map, Placemark } from '@pbe/react-yandex-maps'

import { useScreenHeight, useMediaMinWidth } from '../hooks'

import markIcon from '../images/icon-location.svg'

interface Props {
  lat: number
  lon: number
}

function Maps({ lat, lon }: Props) {
  const screenHeight = useScreenHeight()
  const isDesktopSize = useMediaMinWidth(1024)
  const headerHeight = isDesktopSize ? 280 : 300

  return (
    <YMaps>
      <Map
        state={{ center: [lat + 0.13, lon], zoom: 9 }}
        style={{
          width: '100%',
          height: screenHeight - headerHeight + 'px',
        }}
      >
        <Placemark
          geometry={[lat, lon]}
          defaultOptions={{
            iconLayout: 'default#image',
            iconImageSize: [46, 56],
            iconImageHref: markIcon,
            iconImageOffset: [-23, -56],
          }}
        />
      </Map>
    </YMaps>
  )
}

export default Maps
