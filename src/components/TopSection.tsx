import { useState, useEffect, useRef } from 'react'
import styled, { css, keyframes } from 'styled-components'
import toast from 'react-hot-toast'

import { useMediaMinWidth } from '../hooks'
import { Data } from '../App'
import topBg from '../images/pattern-bg.png'
import submitIcon from '../images/icon-arrow.svg'

interface Props {
  setData: React.Dispatch<React.SetStateAction<Data>>
}

function TopSection({ setData }: Props) {
  const [inputText, setInputText] = useState('')
  const inputRef = useRef<HTMLInputElement>(null)
  const [loading, setLoading] = useState(false)
  const isDesktop = useMediaMinWidth(1024)

  useEffect(() => {
    getQuery('')
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  function submit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    getQuery(inputText)
    setInputText('')
    if (!isDesktop) {
      inputRef.current?.blur()
    }
  }

  function getQuery(query: string) {
    setLoading(true)
    fetch(`/api/getip?ip=${inputText.trim()}`)
      .then(res => res.json())
      .then(data => {
        if (data.status === 'fail') {
          toast.error(data.message[0].toUpperCase() + data.message.slice(1))
          return
        }
        setData({
          ip: data.ip,
          location: data.location,
          timezone: data.timezone,
          isp: data.isp,
          lat: data.lat,
          lon: data.lon,
        })
      })
      .catch(error => {
        console.error(error)
        toast.error(`Error fetching the data: ${error}`)
      })
      .finally(() => setLoading(false))
  }

  return (
    <Container>
      <Heading>IP Address Tracker</Heading>
      <Form action="" onSubmit={submit}>
        <Input
          autoComplete="off"
          type="text"
          name="text"
          placeholder="Search for any IP address or domain"
          aria-label="IP address or domain"
          value={inputText}
          onChange={e => setInputText(e.currentTarget.value)}
          ref={inputRef}
        />
        <SubmitButton type="submit" aria-label="Submit" $loading={loading} />
      </Form>
    </Container>
  )
}

const Container = styled.div`
  background: url(${topBg}) center;
  height: 300px;
  text-align: center;
  background-size: cover;

  @media screen and (min-width: 1024px) {
    height: 280px;
  }
`

const Heading = styled.h1`
  margin: 0;
  padding: 26px 0 0;
  font-family: var(--ff);
  font-weight: 500;
  font-size: 26px;
  letter-spacing: -0.01em;
  color: var(--h1-color);

  @media screen and (min-width: 1024px) {
    padding-top: 29px;
    font-size: 32px;
  }
`

const Form = styled.form`
  margin: 28px auto 0;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  padding: 0 24px;
  max-width: 603px;

  @media screen and (min-width: 1024px) {
    margin-top: 27px;
  }
`

const Input = styled.input`
  margin: 0;
  padding: 0 20px;
  border: none;
  height: 58px;
  flex: 1;
  border-radius: 15px 0 0 15px;
  font-family: var(--ff);
  font-weight: 400;
  font-size: 18px;
  letter-spacing: 0.01em;

  &::placeholder {
    color: var(--light-gray);
    opacity: 1;
  }

  @media screen and (min-width: 1024px) {
    padding: 0 24px;
    letter-spacing: 0;
  }
`

const opacity = keyframes`
  from { opacity: 1 }
  50% { opacity: 0 }
  to { opacity: 1 }
`

const SubmitButton = styled.button<{ $loading: any }>`
  margin: 0;
  padding: 0;
  border: none;
  width: 58px;
  height: 58px;
  border-radius: 0 15px 15px 0;
  background: url(${submitIcon}) no-repeat, black;
  background-position: center;
  cursor: pointer;
  transition: 0.3s;
  ${props =>
    props.$loading &&
    css`
      animation: ${opacity} 1s ease-in-out infinite;
    `}

  &:hover {
    background-color: var(--button-hover);
  }
`

export default TopSection
