import { useEffect, useState } from "react"

function useMediaMinWidth(minWidth: number) {
  const mediaQuery = `screen and (min-width: ${minWidth}px)`
  const [isMediaWidth, setIsMediaWidth] = useState(
    window.matchMedia(mediaQuery).matches
  )

  useEffect(() => {
    function handleResize() {
      setIsMediaWidth(
        window.matchMedia(mediaQuery).matches
      )
    }
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [mediaQuery])

  return isMediaWidth
}

export default useMediaMinWidth
