import { useState } from 'react'
import styled from 'styled-components'
import { Toaster } from 'react-hot-toast'

import TopSection from './components/TopSection'
import Display from './components/Display'
import Maps from './components/Maps'

export interface Data {
  ip: string
  location: string
  timezone: string
  isp: string
  lat: number
  lon: number
}

const initialData: Data = {
  ip: '',
  location: '',
  timezone: '',
  isp: '',
  lat: 11,
  lon: 124,
}

function App() {
  const [data, setData] = useState<Data>(initialData)

  return (
    <Main>
      <Toaster />
      <TopSection setData={setData} />
      <Display
        ip={data.ip}
        location={data.location}
        timezone={data.timezone}
        isp={data.isp}
      />
      <Maps lat={data.lat} lon={data.lon} />
    </Main>
  )
}

const Main = styled.main`
  width: 100%;
  height: 100%;
`

export default App
