const url = require('node:url')
const axios = require('axios')

function formatSecondsOffset(seconds) {
  const sign = seconds < 0 ? '-' : seconds > 0 ? '+' : '±'
  const text = new Date(Math.abs(seconds) * 1000)
    .toISOString()
    .substring(11, 16)
  return sign + text
}

module.exports = (req, res) => {
  const queryObject = url.parse(req.url, true).query
  const queryIP = queryObject.ip || req.headers['x-real-ip']

  axios
    .get(
      `http://ip-api.com/json/${queryIP}?fields=status,message,country,city,zip,lat,lon,offset,isp,query`
    )
    .then(({ data }) => {
      if (data.status === 'fail') {
        res.statusCode = 200
        res.json(data)
        return
      }
      res.statusCode = 200
      res.json({
        ip: data.query,
        location: `${data.city}, ${data.country}${data.zip && ' ' + data.zip}`,
        timezone: 'UTC ' + formatSecondsOffset(Number(data.offset)),
        isp: data.isp,
        lat: data.lat,
        lon: data.lon,
        status: data.status,
      })
    })
    .catch(error => {
      throw new Error(error)
    })
}
