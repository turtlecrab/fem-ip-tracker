# Frontend Mentor - IP address tracker

![Design preview for the IP address tracker coding challenge](./design/desktop-preview.jpg)

## Links

- Live site: <https://fem-ip-tracker-bice.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/ip-address-tracker-w-vercels-serverless-function--gtKF9-wDX>

## Stuff I learned

- using serverless functions on vercel for [simple api](./api/getip.js)
- extracting [custom hooks](./src/hooks)
- using react yandex [maps](./src/components/Maps.tsx)

## Stuff I should've learned but I didn't

- the [fetching code](src/components/TopSection.tsx) looks like a mess. No idea what's the best approach to code such situation where we fetch once after mounting and also fetch by submitting a form. Empty dependency array for `useEffect` looks to me like a correct call, but it also looks like crap.